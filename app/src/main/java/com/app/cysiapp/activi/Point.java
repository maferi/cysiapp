package com.app.cysiapp.activi;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by USUARIO on 24/05/2015.
 */
public class Point {


    private long id;
    private int position;
    private int value;
    String type;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
