package com.app.cysiapp.activi;

import android.content.Intent;
import android.provider.Contacts;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.cysiapp.R;


public class Login extends ActionBarActivity {
        private String[] info;
        Toast toast;
        Intent verify,recovery, intent;
        public static String username;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        info = new String[3];
        verify = new Intent(this, Verification.class );
        recovery = new Intent(this, Recovery.class );
        toast = Toast.makeText(this, "Disculpe debe completar la información", Toast.LENGTH_SHORT);
        DatabaseHandler db=new DatabaseHandler(this);
        if(db.getAllContacts().size()>0){
            Contact c=db.getContact(1);
            if(c.get_conection().equals("no")){setContentView(R.layout.init_login);}
            else{info[0] = c.get_username();
                info[1] = c.get_password();
                info[2]="";
                Verification.rol=c.get_rol();
                verify.putExtra(Verification.ubication,info);
                startActivity(verify);
                finish();}
           }else{db.addContact(new Contact("usuario","contraseña","Admin","no"));
                setContentView(R.layout.init_login);}
    }

    public void register(View view){
        setContentView(R.layout.init_registration);
    }

    public void initiation(View view){setContentView(R.layout.init_login);}

    public void start(View view){
        info[0] = ((EditText) findViewById(R.id.insertReader)).getText().toString();
        info[1] = ((EditText) findViewById(R.id.insertPassword)).getText().toString();
        info[2]="start";
        username=info[0];
        verify.putExtra(Verification.ubication,info);
        if (info[0].equals("") || info[1].equals("")) { toast.show();}
        else if(info[0].equals("admin") && info[1].equals("admin")){
            intent=new Intent(getApplication(),Configure_Access.class);
            intent.putExtra(Verification.ubication,"init");
            startActivity(intent);
        }else {startActivity(verify); finish();}
    }

    public void check(View view){
        info[0] = ((EditText) findViewById(R.id.insertEmail)).getText().toString();
        info[1] = ((EditText) findViewById(R.id.insertCode)).getText().toString();
        info[2]="check";
        verify.putExtra(Verification.ubication,info);
        if (info[0].equals("") || info[1].equals("")) {
            toast.show();
        } else {
            startActivity(verify);
            finish();
        }
    }

    public void recuperate(View view){
        startActivity(recovery);
        overridePendingTransition(R.anim.abc_grow_fade_in_from_bottom, R.anim.abc_shrink_fade_out_from_bottom);

    }

}
