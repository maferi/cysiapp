package com.app.cysiapp.activi;

import android.app.Application;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;

import com.app.cysiapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexis on 31/05/2015.
 */
public class GlobalClass extends Application {

    private List<Variable> variables = new ArrayList<Variable>();
    private int id = 1;
    private NotificationManager n;


    private List<Integer> imagen = new ArrayList<Integer>();

    public GlobalClass() {}

    public List<Variable> getVariables() {
        return variables;
    }

    public void setVariables(List<Variable> variables) {
        this.variables = variables;
    }

    public void setValueVariables(Variable variable){
        this.variables.add(0,variable);
        if(variable.getStatus()==1){imagen.add(0,new Integer(R.drawable.flechahigh));}
        else{imagen.add(0,new Integer(R.drawable.flechalow));}
    }

    public List<Integer> getImagen() {
        return imagen;
    }

    public void setImagen(List<Integer> imagen) {
        this.imagen = imagen;
    }

    public void notificationVal(){
        n = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        android.app.Notification notif = new android.app.Notification(R.drawable.ic_action_name, "Notification Personal", System.currentTimeMillis());
        PendingIntent intent = PendingIntent.getActivity(getApplicationContext(),0, new Intent(getApplicationContext(), Notifications.class), 0);
        //Log.i("mensaje", String.valueOf(id));
        notif.setLatestEventInfo(getApplicationContext(), "Notificacion", "Notificacion personal de android", intent);
        n.notify(id, notif);
        id++;

    }

    public void cancelNotification(){if(id>1) n.cancelAll();}
}
