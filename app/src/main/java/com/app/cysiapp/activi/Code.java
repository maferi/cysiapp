package com.app.cysiapp.activi;

/**
 * Created by Mafer on 28/06/2015.
 */
public class Code {

    private String key;
    private boolean active;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
