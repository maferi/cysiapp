package com.app.cysiapp.activi;


import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.app.cysiapp.R;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by USUARIO on 25/05/2015.
 */
public class Configure_Variable extends ActionBarActivity {

    RestAdapter restAdapter;
    Intent intent,recive;
    String action, type;
    EditText name,posicion,edit_set,pos_set,edit_low,pos_low,edit_high,pos_high;
    TextView title,title_load;
    CheckBox checkboxSet,checkboxLow,checkboxHigh;
    RadioButton sD,sA;
    Toast toast;
    Boolean a;
    long id_variable;
    int vs,ps,vl,pl,vh,ph;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.configure_variable);
        toast=Toast.makeText(this,"",Toast.LENGTH_LONG);
        intent=new Intent(this,View_Variable.class);
        recive=getIntent();
        action=recive.getStringExtra(Verification.ubication);

        edit_set = (EditText) findViewById(R.id.editText);
        pos_set=(EditText) findViewById(R.id.editText2);
        checkboxSet=(CheckBox) findViewById(R.id.checkBox1);

        edit_low = (EditText) findViewById(R.id.editText3);
        pos_low=(EditText) findViewById(R.id.editText4);
        checkboxLow=(CheckBox) findViewById(R.id.checkBox);

        edit_high = (EditText) findViewById(R.id.editText5);
        pos_high=(EditText) findViewById(R.id.editText6);
        checkboxHigh=(CheckBox) findViewById(R.id.checkBox2);

        sD=(RadioButton)findViewById(R.id.selec_digital);
        sA=(RadioButton)findViewById(R.id.selec_analogic);

        restAdapter= new RestAdapter.Builder()
                .setEndpoint(Verification.END_POINT)
                .build();

        if(action.equals("edit")){
            editVariable();
        }else{
            newVariable();
        }
    }

    private void editVariable(){
        title=(TextView)findViewById(R.id.title_config_variable);
        name=(EditText)findViewById(R.id.insert_name_variable);
        posicion=(EditText)findViewById(R.id.insert_posicion_variable);
        a=true;
        title.setText("Editar variable");
        name.setText(Session.list_variable.get(Real_Time.posición_variable).getName());
        posicion.setText(Integer.toString(Session.list_variable.get(Real_Time.posición_variable).getPosition()));
        if(Session.list_variable.get(Real_Time.posición_variable).getType().equals("digital")){
            sD.setChecked(true);
            sA.setEnabled(false);
            sA.setVisibility(View.INVISIBLE);
        }else{
            sA.setChecked(true);
            sD.setEnabled(false);
            sD.setVisibility(View.INVISIBLE);
            checkboxSet.setEnabled(true);
            checkboxSet.setVisibility(View.VISIBLE);
            checkboxLow.setEnabled(true);
            checkboxLow.setVisibility(View.VISIBLE);
            checkboxHigh.setEnabled(true);
            checkboxHigh.setVisibility(View.VISIBLE);
            List<Point> list_point=Session.list_variable.get(Real_Time.posición_variable).getPoint();
            for(int i=0;i<list_point.size();i++){
                if(list_point.get(i).getType().equals("set")){
                    checkboxSet.setChecked(true);
                    edit_set.setEnabled(true);
                    edit_set.setVisibility(View.VISIBLE);
                    edit_set.setText(Integer.toString(list_point.get(i).getValue()));
                    pos_set.setEnabled(true);
                    pos_set.setVisibility(View.VISIBLE);
                    pos_set.setText(Integer.toString((list_point.get(i).getPosition())));}

                else if(list_point.get(i).getType().equals("low")){
                    checkboxLow.setChecked(true);
                    edit_low.setEnabled(true);
                    edit_low.setVisibility(View.VISIBLE);
                    edit_low.setText(Integer.toString(list_point.get(i).getValue()));
                    pos_low.setEnabled(true);
                    pos_low.setVisibility(View.VISIBLE);
                    pos_low.setText(Integer.toString((list_point.get(i).getPosition())));}

                else{checkboxHigh.setChecked(true);
                    edit_high.setEnabled(true);
                    edit_high.setVisibility(View.VISIBLE);
                    edit_high.setText(Integer.toString(list_point.get(i).getValue()));
                    pos_high.setEnabled(true);
                    pos_high.setVisibility(View.VISIBLE);
                    pos_high.setText(Integer.toString((list_point.get(i).getPosition())));}
            }
        }



    }

    private  void newVariable(){
        name=(EditText)findViewById(R.id.insert_name_variable);
        posicion=(EditText)findViewById(R.id.insert_posicion_variable);
        sD.setChecked(true);
        a=false;
    }

    public void cancelConfigVariable(View view){
        finish();
        if(action.equals("edit")){
            startActivity(intent);
        }
    }

    public void saveVariable(View view){
        boolean go=true;
        if (name.getText().toString().equals("") || posicion.getText().toString().equals("")) {
            toast.setText("Debe ingresar el nombre y posición de la variable");
            toast.show();
            go=false;
        }else {
            if(go&&sD.isChecked()){type="digital";}
            else {
                if (go && checkboxSet.isChecked() == true) {
                    if (edit_set.getText().toString().equals("") || pos_set.getText().toString().equals("")) {
                        toast.setText("Debe ingresar el valor y  posición del set");
                        toast.show();
                        go = false;
                    } else if (go && ((pos_set.getText().toString().equals(posicion.getText().toString())) || (checkboxLow.isChecked() == true && pos_set.getText().toString().equals(pos_low.getText().toString())) || (checkboxLow.isChecked() == true && pos_set.getText().toString().equals(pos_high.getText().toString())))) {
                        toast.setText("Error, la posiciones no puede coincidir");
                        toast.show();
                        go = false;
                    }
                }
                if (go && checkboxLow.isChecked() == true) {
                    if (edit_low.getText().toString().equals("") || pos_low.getText().toString().equals("")) {
                        toast.setText("Debe ingresar el valor y  posición del umbral minimo");
                        toast.show();
                        go = false;
                    } else if (go && (pos_low.getText().toString().equals(posicion.getText().toString()) || (checkboxHigh.isChecked() == true && pos_low.getText().toString().equals(pos_high.getText().toString())))) {
                        toast.setText("Error, la posiciones no puede coincidir");
                        toast.show();
                        go = false;
                    }
                }
                if (go && checkboxHigh.isChecked() == true) {
                    if (edit_high.getText().toString().equals("") || pos_high.getText().toString().equals("")) {
                        toast.setText("Debe ingresar el valor y  posición del umbral maximo");
                        toast.show();
                        go = false;
                    } else if (go && (pos_high.getText().toString().equals(posicion.getText().toString()))) {
                        toast.setText("Error, la posiciones no puede coincidir");
                        toast.show();
                        go = false;
                    }
                }
                if (go && checkboxLow.isChecked() == true && checkboxSet.isChecked() == true) {
                    if (Integer.parseInt(edit_set.getText().toString()) <= Integer.parseInt(edit_low.getText().toString())) {
                        toast.setText("Error, el valor del set debe ser mayor al umbral mínimo");
                        toast.show();
                        go = false;
                    }
                }
                if (go && checkboxHigh.isChecked() == true && checkboxSet.isChecked() == true) {
                    if (Integer.parseInt(edit_set.getText().toString()) >= Integer.parseInt(edit_high.getText().toString())) {
                        toast.setText("Error, el valor del set debe ser menor al umbral máximo");
                        toast.show();
                        go = false;
                    }
                }
                if (go && checkboxHigh.isChecked() == true && checkboxLow.isChecked() == true) {
                    if (Integer.parseInt(edit_low.getText().toString()) >= Integer.parseInt(edit_high.getText().toString())) {
                        toast.setText("Error, el valor del umbral mínimo debe ser menor al valor del umbral máximo");
                        toast.show();
                        go = false;
                    }
                }
                type="analógica";
            }
            if(go){
                setContentView(R.layout.loading);
                title = (TextView) findViewById(R.id.textView_veryfi);
                title.setText("Guardando");
                if(go&&checkboxSet.isChecked()==true){ps=Integer.parseInt(pos_set.getText().toString()); vs=Integer.parseInt(edit_set.getText().toString());}
                else{ps=-1;vs=-303030;}
                if(go&&checkboxLow.isChecked()==true){pl=Integer.parseInt(pos_low.getText().toString()); vl=Integer.parseInt(edit_low.getText().toString());}
                else{pl=-1;vl=-303030;}
                if(go&&checkboxHigh.isChecked()==true){ph=Integer.parseInt(pos_high.getText().toString()); vh=Integer.parseInt(edit_high.getText().toString());}
                else{ph=-1;vh=-303030;}

                if(a==false){
                   final ProcessAPI processAPI = restAdapter.create(ProcessAPI.class);
                    processAPI.ConfigureVariable(Long.valueOf(action),name.getText().toString(),Integer.parseInt(posicion.getText().toString()),type,ps,vs,pl,vl,ph,vh,new Callback<ServerResponse>() {
                        @Override
                        public void success(ServerResponse serverResponse, Response response) {
                            if(serverResponse.isOk()){
                            processAPI.ViewAllProcess(new Callback<List<Process>>() {
                                @Override
                                public void success(List<Process> processes, Response response) {
                                    Verification.list_process=processes;
                                    if (Session.p==Long.valueOf(action)){
                                        Session.list_variable = processes.get(Session.position_clik).getVariable();
                                        Session.name_value_variable=new String[Session.list_variable.size()];
                                        Session.name_v=new String[Session.list_variable.size()];
                                        processAPI.RealTime(Session.p,new Callback<ServerResponse>() {
                                            @Override
                                            public void success(ServerResponse serverResponse, Response response) {
                                                for(int i=0;i<Session.list_variable.size();i++){
                                                    Session.name_v[i]=Session.list_variable.get(i).getName();
                                                    if(Session.list_variable.get(i).getType().equals("digital")){ Session.name_value_variable[i]=Session.list_variable.get(i).getName()+System.getProperty(("line.separator"))+Integer.toString(serverResponse.getValue()[Verification.list_process.get(Session.position_clik).getQuantity()+Session.list_variable.get(i).getPosition()]);
                                                    }else{Session.name_value_variable[i]=Session.list_variable.get(i).getName()+System.getProperty(("line.separator"))+Integer.toString(serverResponse.getValue()[Session.list_variable.get(i).getPosition()]);}
                                                    finish();
                                                    toast.setText("Condiguración exitosa");
                                                    toast.show();
                                                }
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {}});
                                    }else{finish();
                                        toast.setText("Condiguración exitosa");
                                        toast.show();}

                                }
                                @Override
                                public void failure(RetrofitError error) {}});

                            }
                            else{toast.setText(serverResponse.getMessage());
                                toast.show();
                                finish();}
                        }

                        @Override
                        public void failure(RetrofitError error) {}});
                }else{
                    id_variable=Session.list_variable.get(Real_Time.posición_variable).getId();
                    type=Session.list_variable.get(Real_Time.posición_variable).getType();
                    final ProcessAPI processAPI = restAdapter.create(ProcessAPI.class);
                    processAPI.EditVariable(id_variable, name.getText().toString(), Integer.parseInt(posicion.getText().toString()),type,ps,vs,pl,vl,ph,vh,new Callback<ServerResponse>() {
                        @Override
                        public void success(ServerResponse serverResponse, Response response) {
                            if (serverResponse.isOk()) {
                                processAPI.ViewListVariable(Session.p, new Callback<List<Variable>>() {
                                    @Override
                                    public void success(List<Variable> variables, Response response) {
                                        Session.list_variable.clear();
                                        Session.list_variable = variables;
                                        Session.name_v=new String[variables.size()];
                                        Session.name_value_variable = new String[Session.list_variable.size()];
                                        for (int i = 0; i < Session.list_variable.size(); i++) {
                                            if( Session.list_variable.get(i).getId()==id_variable){Real_Time.posición_variable=i;}
                                            Session.name_v[i]=variables.get(i).getName();
                                            Session.name_value_variable[i] = Session.list_variable.get(i).getName() +
                                                    System.getProperty(("line.separator")) +
                                                    Integer.toString(1);
                                        }
                                        finish();
                                        startActivity(intent);
                                        toast.setText("Condiguración exitosa");
                                        toast.show();
                                    }
                                    @Override
                                    public void failure(RetrofitError error) {Log.d("retrofit", error.toString());}});
                            } else {
                                toast.setText(serverResponse.getMessage());
                                toast.show();
                                setContentView(R.layout.configure_variable);
                                editVariable();
                            }
                        }
                        @Override
                        public void failure(RetrofitError error) {Log.d("retrofit", error.toString());}});
                }
            }
        }
    }

    public void selecSet(View view){
        if(checkboxSet.isChecked()==true){
            edit_set.setEnabled(true);
            edit_set.setVisibility(View.VISIBLE);
            pos_set.setEnabled(true);
            pos_set.setVisibility(View.VISIBLE);}
        else{
            edit_set.setText("");
            edit_set.setEnabled(false);
            edit_set.setVisibility(View.INVISIBLE);
            pos_set.setText("");
            pos_set.setEnabled(false);
            pos_set.setVisibility(View.INVISIBLE);
        }
    }

    public void selecLow(View view){
        if(checkboxLow.isChecked()==true){
            edit_low.setEnabled(true);
            edit_low.setVisibility(View.VISIBLE);
            pos_low.setEnabled(true);
            pos_low.setVisibility(View.VISIBLE);}
        else{
            edit_low.setText("");
            edit_low.setEnabled(false);
            edit_low.setVisibility(View.INVISIBLE);
            pos_low.setText("");
            pos_low.setEnabled(false);
            pos_low.setVisibility(View.INVISIBLE);
        }
    }

    public void selecHigh(View view){
        if(checkboxHigh.isChecked()==true){
            edit_high.setEnabled(true);
            edit_high.setVisibility(View.VISIBLE);
            pos_high.setEnabled(true);
            pos_high.setVisibility(View.VISIBLE);}
        else{
            edit_high.setText("");
            edit_high.setEnabled(false);
            edit_high.setVisibility(View.INVISIBLE);
            pos_high.setText("");
            pos_high.setEnabled(false);
            pos_high.setVisibility(View.INVISIBLE);
        }
    }

    public void selecDigital(View view){
        sA.setChecked(false);
        checkboxSet.setChecked(false);
        checkboxSet.setEnabled(false);
        checkboxSet.setVisibility(View.INVISIBLE);
        checkboxLow.setChecked(false);
        checkboxLow.setEnabled(false);
        checkboxLow.setVisibility(View.INVISIBLE);
        checkboxHigh.setChecked(false);
        checkboxHigh.setEnabled(false);
        checkboxHigh.setVisibility(View.INVISIBLE);

        edit_high.setEnabled(false);
        edit_high.setVisibility(View.INVISIBLE);
        pos_high.setEnabled(false);
        pos_high.setVisibility(View.INVISIBLE);

        edit_low.setEnabled(false);
        edit_low.setVisibility(View.INVISIBLE);
        pos_low.setEnabled(false);
        pos_low.setVisibility(View.INVISIBLE);

        edit_set.setEnabled(false);
        edit_set.setVisibility(View.INVISIBLE);
        pos_set.setEnabled(false);
        pos_set.setVisibility(View.INVISIBLE);
    }

    public void selecAnalogic(View view){
        sD.setChecked(false);
        checkboxSet.setEnabled(true);
        checkboxSet.setVisibility(View.VISIBLE);
        checkboxLow.setEnabled(true);
        checkboxLow.setVisibility(View.VISIBLE);
        checkboxHigh.setEnabled(true);
        checkboxHigh.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if(keyCode==KeyEvent.KEYCODE_BACK){
            finish();
            if(action.equals("edit")){
                startActivity(intent);
            }

        }
        return super.onKeyDown(keyCode, event);
    }



}
