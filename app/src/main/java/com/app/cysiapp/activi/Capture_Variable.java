package com.app.cysiapp.activi;

import java.util.Date;

/**
 * Created by USUARIO on 01/06/2015.
 */
public class Capture_Variable {

    private int value;

    private long date;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }
}
