package com.app.cysiapp.activi;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by USUARIO on 19/05/2015.
 */
public interface ReaderAPI {

    @GET("/start/checkup")
    public void Verification(@Query("email") String email,
                             @Query("key") String key,
                             Callback<ServerResponse> response);

    @GET("/registration")
    public void Registration(@Query("name") String name,
                             @Query("surname") String surname,
                             @Query("charter") String charter,
                             @Query("username") String username,
                             @Query("password") String password,
                             @Query("email") String email,
                             @Query("id_code") long id_code,
                             Callback<ServerResponse> response);

    @GET("/recovery/{user_password}")
    public void Recovery(@Path("user_password") String user_password,
                         @Query("email") String email,
                         Callback<ServerResponse> response);

    @GET("/start/session")
    public void StartSesion(@Query("username") String username,
                            @Query("password") String password,
                            Callback<ServerResponse> response);

    @GET("/change/email")
    public void ChangeEmail(@Query("current_email") String current_email,
                            @Query("new_email") String new_email,
                            @Query("password") String password,
                            Callback<ServerResponse> response);

    @GET("/change/password")
    public void ChangePassword(@Query("username") String username,
                               @Query("current_password") String current_password,
                               @Query("new_password") String new_password,
                               Callback<ServerResponse> response);
}
