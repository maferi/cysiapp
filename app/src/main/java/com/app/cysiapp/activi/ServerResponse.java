package com.app.cysiapp.activi;

/**
 * Created by USUARIO on 19/05/2015.
 */
public class ServerResponse {

    boolean ok;
    String message;
    long id;
    String[] chain;
    int[] value;

    public boolean isOk() {
        return ok;
    }

    public String getMessage() {
        return message;
    }

    public long getId() {
        return id;
    }

    public String[] getChain() {
        return chain;
    }

    public int[] getValue() {
        return value;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setChain(String[] chain) {
        this.chain = chain;
    }

    public void setValue(int[] value) {
        this.value = value;
    }
}
