package com.app.cysiapp.activi;

import android.widget.EditText;

import java.util.Date;
import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by USUARIO on 20/05/2015.
 */
public interface ProcessAPI {

    @GET("/view/list/process")
    public void ViewListProcess(@Query("username") String username,
                               Callback<List<Process>> response);

    @GET("/view/all/process")
    public void ViewAllProcess(Callback<List<Process>> response);

    @GET("/view/list/variable")
    public void ViewListVariable(@Query("id_process") long id_process,
                                 Callback<List<Variable>> response);

    @GET("/real/time")
    public void RealTime(@Query("id_process") long id_process,
                                Callback<ServerResponse> response);

    @GET("/remove/variable")
    public void RemoveVariable(@Query("id_variable") long id_variable,
                               Callback<List<Variable>> response);

    @GET("/edit/variable")
    public void EditVariable(@Query("id_variable") long id_variable,
                             @Query("name") String name,
                             @Query("position") int position,
                             @Query("type") String type,
                             @Query("position_set") int position_set,
                             @Query("value_set") int value_set,
                             @Query("position_lowest") int position_lowest,
                             @Query("value_lowest") int value_lowest,
                             @Query("position_highest") int position_highest,
                             @Query("value_highest") int value_highest,
                             Callback<ServerResponse> response);

    @GET("/configure/variable")
    public void ConfigureVariable(@Query("id_process") long id_process,
                                  @Query("name") String name,
                                  @Query("position") int position,
                                  @Query("type") String type,
                                  @Query("position_set") int position_set,
                                  @Query("value_set") int value_set,
                                  @Query("position_lowest") int position_lowest,
                                  @Query("value_lowest") int value_lowest,
                                  @Query("position_highest") int position_highest,
                                  @Query("value_highest") int value_highest,
                                  Callback<ServerResponse> response);

    @GET("/remove/process")
    public void RemoveProcess(@Query("id_process") long id_process,
                               Callback<List<Process>> response);

    @GET("/configure/plc")
    public void ConfigurePlc(@Query("name") String name,
                             @Query("ip") String ip,
                             @Query("slave") int slave,
                             Callback<ServerResponse> response);

    @GET("/remove/plc")
    public void DeletePlc(@Query("name") String name,
                          Callback<ServerResponse> response);

    @GET("/configure/process")
    public void ConfigureProcess(@Query("name") String name,
                                 @Query("description") String description,
                                 @Query("init") int init,
                                 @Query("quantity") int quantity,
                                 @Query("initb") int initb,
                                 @Query("quantityb") int quantityb,
                                 @Query("name_plc") String name_plc,
                                 Callback<ServerResponse> response);

    @GET("/configure/access")
    public void ConfigureAccess(@Query("process") String process,
                                Callback<ServerResponse> response);

    @GET("/listCaptuerVar1")
    public void ViewCapture(@Query("id_variable") long id_variable,
                            @Query("date_init") Date date_init,
                            @Query("date_end") Date date_end,
                            Callback<List<Capture_Variable>>  response);

    @GET("/listCaptuerVar2")
    public void ViewCapture2(@Query("id_variable") long id_variable,
                             @Query("date_init") long date_init,
                             @Query("date_end") long date_end,
                             Callback<List<Capture_Variable>>  response);

    @GET("/edit/plc")
    public void EditPlc(@Query("current_name") String current_name,
                        @Query("name") String name,
                        @Query("ip") String ip,
                        @Query("slave") int slave,
                        Callback<ServerResponse> response);

    @GET("/edit/process")
    public void EditProcess(@Query("current_name") String current_name,
                            @Query("name") String name,
                            @Query("description") String description,
                            @Query("init") int init,
                            @Query("quantity") int quantity,
                            @Query("initb") int initb,
                            @Query("quantityb") int quantityb,
                            @Query("name_plc") String name_plc,
                            Callback<ServerResponse> response);

    @GET("/view/code")
    public void ViewCode(Callback<List<Code>> response);

    @GET("/remove/access")
    public void RemoveAccess(@Query("key") String key,
                             Callback<ServerResponse> response);

    @GET("/perform/task")
    public void PerformTask(@Query("value") int value,
                            @Query("variable_id") long variable_id,
                            Callback<ServerResponse> response);

    @GET("/alert")
    public void AlertCaptureVariable(@Query("username") String username,
                                     @Query("rol") String rol,
                                     Callback <List<Variable>> variable);


}

