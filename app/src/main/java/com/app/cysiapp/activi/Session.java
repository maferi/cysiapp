package com.app.cysiapp.activi;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.cysiapp.R;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by USUARIO on 20/05/2015.
 */
public class Session extends ActionBarActivity {

    RestAdapter restAdapter;
    Intent intent, info;
    ArrayAdapter<String> adapter;
    public static String[] name_value_variable;
    public static String[] name_v;
    public static int[] position_variable;
    public  static List<Variable> list_variable;
    public static long p;
    public static int position_clik;
    private int position_c;
    private long p0;
    AlertDialog.Builder builder;
    DatabaseHandler db=new DatabaseHandler(this);
    Toast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_list_process);
        final Intent intent2 = new Intent(Session.this,Servicio.class);
        startService(intent2);

        toast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
        builder = new AlertDialog.Builder(this);
        builder.setTitle(" Advertencia")
                .setMessage("¿Desea cerrar sesión?")
                .setIcon(getResources().getDrawable(android.R.drawable.ic_dialog_alert))
                .setPositiveButton(android.R.string.yes,new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        db.updateContact(new Contact(1,"no"));
                        stopService(intent2);
                        finish();
                        intent=new Intent(getApplicationContext(),Login.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);}})
                .setNegativeButton(android.R.string.no,new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }});

        intent=new Intent(this,Real_Time.class);
        adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,Verification.name_process);
        ListView lv= (ListView) findViewById(R.id.listViewProcess);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                position_clik=position;
                p=Verification.list_process.get(position).getId();
                list_variable=Verification.list_process.get(position).getVariable();
                loadingProcess();

            }
        });


    }

    public void loadingProcess(){
        setContentView(R.layout.loading);
        TextView textView=(TextView)findViewById(R.id.textView_veryfi);
        textView.setText("Cargando");
        restAdapter= new RestAdapter.Builder().setEndpoint(Verification.END_POINT).build();
        ProcessAPI processAPI  = restAdapter.create(ProcessAPI.class);
        processAPI.RealTime(p,new Callback<ServerResponse>() {
            @Override
            public void success(ServerResponse serverResponse, Response response) {
                name_value_variable=new String[list_variable.size()];
                name_v=new String[list_variable.size()];
                for(int i=0;i<list_variable.size();i++){
                    name_v[i]=list_variable.get(i).getName();
                    if(list_variable.get(i).getType().equals("digital")){
                        name_value_variable[i]=list_variable.get(i).getName()+
                        System.getProperty(("line.separator"))+
                        Integer.toString(serverResponse.getValue()[Verification.list_process.get(position_clik).getQuantity()+list_variable.get(i).getPosition()]);}
                    else{
                         name_value_variable[i]=list_variable.get(i).getName()+
                         System.getProperty(("line.separator"))+
                         Integer.toString(serverResponse.getValue()[list_variable.get(i).getPosition()]);}
                }
                finish();
                startActivity(intent);}

            @Override
            public void failure(RetrofitError error) { Log.d("retrofit", error.toString());}});
    }

    public void leaveViewListProcess(View view) {
         if (!getIntent().getStringExtra(Verification.ubication).equals("change")) {builder.show();}
        else {finish();  startActivity(intent);}
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if(keyCode==KeyEvent.KEYCODE_BACK){
            if(!getIntent().getStringExtra(Verification.ubication).equals("change")){builder.show();}
            else{finish(); startActivity(intent);}
        }
        return super.onKeyDown(keyCode, event);
    }
}
