package com.app.cysiapp.activi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


import com.app.cysiapp.R;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Edit_Value_Real_Time extends Activity {
    RestAdapter restAdapter;
    Intent intent;
    EditText editText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit__value__real__time);
        restAdapter=new RestAdapter.Builder().setEndpoint(Verification.END_POINT).build();
        intent=getIntent();
        editText=(EditText)findViewById(R.id.value_variable_send);
    }

    public void cancel_task(View view){
        finish();
    }

    public void  send_task(View view){
        if(editText.getText().toString().equals("")){Toast.makeText(this, "Disculpe debe ingresar un valor", Toast.LENGTH_SHORT).show();}
        else{setContentView(R.layout.sending);
             ProcessAPI processAPI=restAdapter.create(ProcessAPI.class);
             processAPI.PerformTask(Integer.parseInt(editText.getText().toString()),intent.getLongExtra(Verification.ubication,1),new Callback<ServerResponse>() {
                @Override
                public void success(ServerResponse serverResponse, Response response) {finish();}
                @Override
                public void failure(RetrofitError error) {}
             });
        }

    }
}
