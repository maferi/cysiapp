package com.app.cysiapp.activi;

import java.util.List;

/**
 * Created by USUARIO on 20/05/2015.
 */
public class Process {

    long id;
    String name;
    String description;
    int init;
    int quantity;
    int initb;
    int quantityb;
    Plc plc;
    private List<Variable> variable;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getInit() {
        return init;
    }

    public void setInit(int init) {
        this.init = init;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getInitb() {
        return initb;
    }

    public void setInitb(int initb) {
        this.initb = initb;
    }

    public int getQuantityb() {
        return quantityb;
    }

    public void setQuantityb(int quantityb) {
        this.quantityb = quantityb;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Plc getPlc() {
        return plc;
    }

    public void setPlc(Plc plc) {
        this.plc = plc;
    }

    public List<Variable> getVariable() {
        return variable;
    }

    public void setVariable(List<Variable> variable) {
        this.variable = variable;
    }
}
