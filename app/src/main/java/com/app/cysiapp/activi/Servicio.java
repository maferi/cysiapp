package com.app.cysiapp.activi;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.app.cysiapp.R;

import org.apache.commons.logging.Log;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.http.Query;

public class Servicio extends Service {
    private boolean isStart = false;
    DatabaseHandler db=new DatabaseHandler(this);
    Contact c;

    public Servicio() {
    }

    NotificationManager n;
//    private static  final int ID_N = 1;
//    private int id = 1;

    private Timer timer = new Timer();

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        n = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        c=db.getContact(1);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if(!this.isStart) {
//            final GlobalClass globalVariable = (GlobalClass) getApplicationContext();
//            android.util.Log.i("Local Service", globalVariable.getA());
           // Toast.makeText(this, "Servicio de Notificaciones Activo", Toast.LENGTH_SHORT).show();
            this.isStart = true;
            start();

        }else{
            //Toast.makeText(this, "Este Servicio ya se Encuentra Activo ",Toast.LENGTH_SHORT).show();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        timer.cancel();
        //Toast.makeText(this, "Servicio de Notificaciones Inactivo",Toast.LENGTH_SHORT).show();
        this.isStart=false;
        super.onDestroy();
    }

    @Override
    public ComponentName startService(Intent service) {
        return super.startService(service);
    }

    @Override
    public boolean stopService(Intent name) {
        return super.stopService(name);
    }

    private void start(){
        timer.scheduleAtFixedRate(task, 5000,60000);
    }
    Random rnd = new Random();
    private TimerTask task = new TimerTask() {
        @Override
        public void run() {
            alert();
//            int intRnd = rnd.nextInt();
//            if(intRnd>=0) {
//                setArrayVal(String.valueOf(intRnd));
//            }
//            else{
//               // mensaje(intRnd,"-",id);
//            }

        }
    };

    public void mensaje(int msg,String character, int id){
        String msg1 = String.valueOf(msg);
        String id1 = String.valueOf(id);
        android.util.Log.d(msg1, "ESTE ES EL VALOR DE RANDON"+character+id1);
    }

    public void setArrayVal(Variable val){
       final GlobalClass globalVariable = (GlobalClass) getApplicationContext();
       globalVariable.setValueVariables(val);
       globalVariable.notificationVal();

    }

    public void alert(){
        RestAdapter restAdapter;
        restAdapter = new RestAdapter.Builder().setEndpoint(Verification.END_POINT).build();
        ProcessAPI processAPI = restAdapter.create(ProcessAPI.class);
        processAPI.AlertCaptureVariable(c.get_username(),c.get_rol(),new Callback<List<Variable>>() {
            @Override
            public void success(List<Variable> variables, Response response) {
                android.util.Log.i("longitud de la lista",String.valueOf(variables.size()));
                for (int i=0; i<variables.size(); i++){
                    android.util.Log.i("msg",variables.get(i).getName());
                    setArrayVal(variables.get(i));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                android.util.Log.e("Error", error.toString());
            }
        });

    }
}
