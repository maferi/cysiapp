package com.app.cysiapp.activi;


/**
 * Created by Mafer on 06/07/2015.
 */
public class Contact {

    //private variables
    int _id;
    String _username;
    String _password;
    String _rol;
    String _conection;

    // Empty constructor
    public Contact(){

    }
    // constructor

    public Contact(int _id, String _username, String _password, String _rol, String _conection) {
        this._id = _id;
        this._username = _username;
        this._password = _password;
        this._rol = _rol;
        this._conection = _conection;
    }

    public Contact(int _id, String _conection) {
        this._id = _id;
        this._conection = _conection;
    }

    public Contact(String _password, String _username, String _rol, String _conection) {
        this._password = _password;
        this._username = _username;
        this._rol = _rol;
        this._conection = _conection;
    }

    public Contact(String _username, String _password) {
        this._username = _username;
        this._password = _password;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_username() {
        return _username;
    }

    public void set_username(String _username) {
        this._username = _username;
    }

    public String get_password() {
        return _password;
    }

    public void set_password(String _password) {
        this._password = _password;
    }

    public String get_rol() {
        return _rol;
    }

    public void set_rol(String _rol) {
        this._rol = _rol;
    }

    public String get_conection() {
        return _conection;
    }

    public void set_conection(String _conection) {
        this._conection = _conection;
    }
}
