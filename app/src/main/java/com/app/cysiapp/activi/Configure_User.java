package com.app.cysiapp.activi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.cysiapp.R;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by USUARIO on 27/05/2015.
 */
public class Configure_User extends ActionBarActivity {
    Intent entry;
    RestAdapter restAdapter;
    TextView title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        entry=getIntent();
        String a=entry.getStringExtra(Verification.ubication);
        restAdapter= new RestAdapter.Builder().setEndpoint(Verification.END_POINT).build();

        if(a.equals("email")){
            setContentView(R.layout.change_email);
        }else{
            setContentView(R.layout.change_password);
        }

    }


    public void leaveChangeEmail(View view){
         finish();
    }

    public void saveChangeEmail(View view){
        String current_email=((EditText)findViewById(R.id.current_email)).getText().toString(),
               new_email=((EditText)findViewById(R.id.new_email)).getText().toString(),
               password=((EditText)findViewById(R.id.password)).getText().toString();

        if(current_email.equals("")||new_email.equals("")||password.equals("")){
            Toast.makeText(this,"Debe llenar todos los campos",Toast.LENGTH_SHORT).show();
        }else{
            setContentView(R.layout.loading);
            title=(TextView)findViewById(R.id.textView_veryfi);
            title.setText("Guardando");
            ReaderAPI readerAPI = restAdapter.create(ReaderAPI.class);
            readerAPI.ChangeEmail(current_email,new_email,password,new Callback<ServerResponse>() {
                @Override
                public void success(ServerResponse serverResponse, Response response) {
                    Toast.makeText(getApplicationContext(),serverResponse.getMessage(),Toast.LENGTH_LONG).show();
                    finish();
                }
                @Override
                public void failure(RetrofitError error) {Log.d("retrofit", error.toString());}
            });
        }


    }

    public void leaveChangePassword(View view){
        finish();
    }

    public void saveChangePassword(View view){
        String username=Login.username,
               current_password=((EditText)findViewById(R.id.current_password)).getText().toString(),
               new_password=((EditText)findViewById(R.id.new_password)).getText().toString(),
               rep_new_password=((EditText)findViewById(R.id.rep_new_password)).getText().toString();

        if(username.equals("")||current_password.equals("")||new_password.equals("")||rep_new_password.equals("")){
            Toast.makeText(this,"Debe llenar todos los campos",Toast.LENGTH_SHORT).show();
        }else{
            if(new_password.equals(rep_new_password)) {
                setContentView(R.layout.loading);
                title = (TextView) findViewById(R.id.textView_veryfi);
                title.setText("Guardando");
                ReaderAPI readerAPI = restAdapter.create(ReaderAPI.class);
                readerAPI.ChangePassword(username,current_password,new_password,new Callback<ServerResponse>() {
                    @Override
                    public void success(ServerResponse serverResponse, Response response) {
                        Toast.makeText(getApplicationContext(),serverResponse.getMessage(),Toast.LENGTH_SHORT).show();
                        finish();
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
            }else{
                Toast.makeText(this,"Las contraseñas no coinciden",Toast.LENGTH_SHORT).show();
            }
        }

    }




}
