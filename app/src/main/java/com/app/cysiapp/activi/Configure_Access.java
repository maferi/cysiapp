package com.app.cysiapp.activi;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.internal.widget.AdapterViewCompat;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.app.cysiapp.R;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by USUARIO on 29/05/2015.
 */
public class Configure_Access extends ActionBarActivity {
    ArrayAdapter<String> adapter;
    ListViewAdapter adapter1;
    RestAdapter restAdapter;
    AlertDialog.Builder builder;
    ListView lv;
    RadioButton rA,rS;
    Boolean type=true;
    Intent entry;
    String a, key;
    ServerResponse r;
    ProcessAPI processAPI;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        entry=getIntent();
        a=entry.getStringExtra(Verification.ubication);
        restAdapter= new RestAdapter.Builder()
                .setEndpoint(Verification.END_POINT)
                .build();
        processAPI  = restAdapter.create(ProcessAPI.class);
        if(a.equals("see")){seeAccess();}
        else if(a.equals("new")||a.equals("init")){newAccess();}


        builder = new AlertDialog.Builder(this);
        builder.setTitle(" Información")
                .setIcon(getResources().getDrawable(android.R.drawable.ic_dialog_info))
                .setNeutralButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        builder = new AlertDialog.Builder(this);
        builder.setTitle(" Advertencia")
                .setMessage("¿Desea eliminar el acceso?")
                .setIcon(getResources().getDrawable(android.R.drawable.ic_dialog_alert))
                .setPositiveButton(android.R.string.yes,new DialogInterface.OnClickListener() {
                     @Override
                     public void onClick(DialogInterface dialog, int which) {
                         setContentView(R.layout.loading);
                         TextView t= (TextView)findViewById(R.id.textView_veryfi);
                         t.setText("Eliminando acceso");
                         processAPI.RemoveAccess(key,new Callback<ServerResponse>() {
                             @Override
                             public void success(ServerResponse serverResponse, Response response) {
                                 processAPI.ViewCode(new Callback<List<Code>>() {
                                     @Override
                                     public void success(List<Code> codes, Response response) {
                                         Verification.list_code=new String[codes.size()];
                                         Verification.img=new int[codes.size()];
                                         for(int i=0;i<codes.size();i++){
                                             Verification.list_code[i]=codes.get(i).getKey();
                                             if(codes.get(i).isActive()){
                                                 Verification.img[i]=R.drawable.busy;
                                             }else{
                                                 Verification.img[i]=R.drawable.a;
                                             }
                                         }
                                         finish();
                                     }

                                     @Override
                                     public void failure(RetrofitError error) {

                                     }
                                 });
                                 }
                             @Override
                             public void failure(RetrofitError error) {}
                         });

                     }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {dialog.cancel();}
                });

   }

   public void seeAccess(){
        setContentView(R.layout.view_list_access);
        ListView lista = (ListView) findViewById(R.id.listView1);
        adapter1 = new ListViewAdapter(this, Verification.list_code, Verification.img);
        lista.setAdapter(adapter1);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                key = Verification.list_code[position];
                builder.create().show();
            }
        });


    }

    public void newAccess(){
        setContentView(R.layout.configure_access);
        adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_multiple_choice,Verification.name_process);
        lv= (ListView) findViewById(R.id.listView2);
        lv.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        rA=(RadioButton)findViewById(R.id.radioButton_Admin);
        rS=(RadioButton)findViewById(R.id.radioButton_Sup);
        rA.setChecked(true);
    }

    public void generateAccess(View view){

        if(type){
            setContentView(R.layout.loading);
            TextView textView=(TextView)findViewById(R.id.textView_veryfi);
            textView.setText("Generando código");
            processAPI.ConfigureAccess("",new Callback<ServerResponse>() {
                @Override
                public void success(ServerResponse serverResponse, Response response) {
                   processAPI.ViewCode(new Callback<List<Code>>() {
                        @Override
                        public void success(List<Code> codes, Response response) {
                            Verification.list_code=new String[codes.size()];
                            Verification.img=new int[codes.size()];
                            for(int i=0;i<codes.size();i++){
                                Verification.list_code[i]=codes.get(i).getKey();
                                if(codes.get(i).isActive()){
                                    Verification.img[i]=R.drawable.busy;
                                }else{
                                    Verification.img[i]=R.drawable.a;
                                }
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {

                        }
                    });
                    setContentView(R.layout.view_code);
                    TextView code=(TextView)findViewById(R.id.code_access);
                    code.setText("Su código de acceso es:"+System.getProperty(("line.separator"))+System.getProperty(("line.separator"))+serverResponse.getMessage());
                }
                @Override
                public void failure(RetrofitError error) {

                }
            });

        }else {
            SparseBooleanArray checked = lv.getCheckedItemPositions();
            String names_process ="";
            ArrayList<String> selectedItems = new ArrayList<String>();
            for (int i = 0; i < checked.size(); i++) {
                // Item position in adapter
                int position = checked.keyAt(i);
                // Add sport if it is checked i.e.) == TRUE!
                if (checked.valueAt(i)) {
                    selectedItems.add(adapter.getItem(position));
                    names_process=names_process+adapter.getItem(position)+" ";
                }
            }
            if (names_process.equals("")) {
                Toast.makeText(this, "Debe seleccionar al menos un proceso", Toast.LENGTH_SHORT).show();
            }else{
                setContentView(R.layout.loading);
                TextView textView=(TextView)findViewById(R.id.textView_veryfi);
                textView.setText("Generando código");
                processAPI.ConfigureAccess(names_process,new Callback<ServerResponse>() {
                    @Override
                    public void success(ServerResponse serverResponse, Response response) {
                        r=serverResponse;
                          processAPI.ViewCode(new Callback<List<Code>>() {
                          @Override
                          public void success(List<Code> codes, Response response) {
                              Verification.list_code=new String[codes.size()];
                              Verification.img=new int[codes.size()];
                              for(int i=0;i<codes.size();i++){
                                  Verification.list_code[i]=codes.get(i).getKey();
                                  if(codes.get(i).isActive()){
                                      Verification.img[i]=R.drawable.busy;
                                  }else{
                                      Verification.img[i]=R.drawable.a;
                                  }
                              }
                              setContentView(R.layout.view_code);
                              TextView code=(TextView)findViewById(R.id.code_access);
                              code.setText("Su código de acceso es:"+System.getProperty(("line.separator"))+System.getProperty(("line.separator"))+r.getMessage());
                          }

                          @Override
                          public void failure(RetrofitError error) {}});

                    }
                    @Override
                    public void failure(RetrofitError error) {}});
            }
        }
    }

    public void checkSup(View view){
        type=false;
        rA.setChecked(false);
        lv.setAdapter(adapter);
    }

    public void checkAdm(View view){
        type=true;
        rS.setChecked(false);
        lv.setAdapter(null);
    }

    public void acceptCode(View view){
        finish();
    }

    public void cancelConfigAccess(View view){
        finish();
    }

    public void leaveViewListAccess(View view){ finish(); }

}
