package com.app.cysiapp.activi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.app.cysiapp.R;

import java.util.List;

/**
 * Created by Alexis on 13/06/2015.
 */
public class ListViewVariableAdapter extends BaseAdapter {

    private Context context;
    private List<Variable> variables;
    LayoutInflater inflater;
    private List<Integer> imagen;

    public ListViewVariableAdapter(Context context, List<Variable> variables,List<Integer> imagen) {
        this.context = context;
        this.variables = variables;
        this.imagen = imagen;
    }

    @Override
    public int getCount() {
        return variables.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TextView txtTitle;
        ImageView img;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.list_row, parent, false);
        txtTitle = (TextView) itemView.findViewById(R.id.textTitle);
        img = (ImageView) itemView.findViewById(R.id.list_row);
        txtTitle.setText(variables.get(position).getName());
        img.setImageResource(imagen.get(position).intValue());
        return itemView;
    }
}
