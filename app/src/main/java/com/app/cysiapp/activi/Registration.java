package com.app.cysiapp.activi;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.cysiapp.R;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class Registration extends ActionBarActivity {
    RestAdapter restAdapter;
    String date[],email;
    Intent register,login;
    Toast toast;
    AlertDialog.Builder builder,builder2;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);
        date=new String[6];
        restAdapter= new RestAdapter.Builder()
                .setEndpoint(Verification.END_POINT)
                .build();

        register=getIntent();
        email=register.getStringExtra(Verification.ubication);
        login=new Intent(this,Login.class);


        toast= Toast.makeText(this,"", Toast.LENGTH_SHORT);
        builder = new AlertDialog.Builder(this);
        builder.setTitle(" Información")
                .setIcon(getResources().getDrawable(android.R.drawable.ic_dialog_info))
                .setNeutralButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                              dialog.cancel();
                            }
                        });
        builder2 = new AlertDialog.Builder(this);
        builder2.setTitle(" Advertencia")
                .setMessage("¿Desea salir del registro?")
                .setIcon(getResources().getDrawable(android.R.drawable.ic_dialog_alert))
                .setPositiveButton(android.R.string.yes,new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
                startActivity(login);
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
            }})
                .setNegativeButton(android.R.string.no,new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }});
    }

    public void saveRegister(View view){
        date[0]=((EditText)findViewById(R.id.insert_name)).getText().toString();
        date[1]=((EditText)findViewById(R.id.insert_surname)).getText().toString();
        date[2]=((EditText)findViewById(R.id.insert_charter)).getText().toString();
        date[3]=((EditText)findViewById(R.id.insert_username)).getText().toString();
        date[4]=((EditText)findViewById(R.id.insert_password)).getText().toString();
        date[5]=((EditText)findViewById(R.id.insert_repeat_password)).getText().toString();

        if(date[0].equals("")||date[1].equals("")||date[2].equals("")||date[3].equals("")||date[4].equals("")||date[5].equals("")){
            toast= Toast.makeText(this, "Disculpe todos los campos son obligatorios", Toast.LENGTH_SHORT);
            toast.show();}
        else if(date[3].equals("admin")||date[3].equals("Admin")){
            toast= Toast.makeText(this, "Nombre de usuario restringido", Toast.LENGTH_SHORT);
            toast.show();}
        else if(date[4].equals(date[5])){
            save();
        }else{
            toast= Toast.makeText(this, "Las contraseñas no coinciden", Toast.LENGTH_SHORT);
            toast.show();}
    }

    public void save(){
        setContentView(R.layout.loading);
        TextView title = (TextView) findViewById(R.id.textView_veryfi);
        title.setText("Guardando");
        ReaderAPI api = restAdapter.create(ReaderAPI.class);
        api.Registration(date[0],date[1],date[2],date[3],date[4],email,Verification.id_code, new Callback<ServerResponse>() {
            @Override
            public void success(ServerResponse serverResponse, Response response) {
                if(serverResponse.isOk()){
                 toast.setText(serverResponse.getMessage());
                 toast.show();
                 finish();
                 startActivity(login);
                }
                else{
                    builder.setMessage(serverResponse.getMessage()).create().show();
                }
            }
            @Override
            public void failure(RetrofitError error) {Log.d("retrofit", error.toString());}
        });
    }

    public void cancel(View view){
      builder2.create().show();
    }

}
