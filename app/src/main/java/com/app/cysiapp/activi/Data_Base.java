package com.app.cysiapp.activi;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;
import com.androidplot.xy.XYStepMode;
import com.app.cysiapp.R;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by USUARIO on 24/05/2015.
 */
public class Data_Base extends ActionBarActivity implements AdapterView.OnItemSelectedListener {
    Intent intent;
    Spinner spinner;
    ArrayAdapter<String> adapter,adapter1;
    private Calendar cal;
    private int day,num[];
    private int month;
    private int year;
    private int hour;
    private int min;
    private String value[],name_varible_selec;
    private EditText et, et1,ef,ef1;
    private String dateInString1, dateInString2,timeInString1,timeInString2,actual;
    SimpleDateFormat sdf,sdf1,sdf2;
    int f;
    long id_variable;
    RestAdapter restAdapter;

    private XYPlot mySimpleXYPlot;
    ArrayList<Double> vector = new ArrayList<Double>();
    double datoX, datoY;
    private PointF minXY;
    private PointF maxXY;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureInit();
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        sdf2 = new SimpleDateFormat("HH:mm:ss");
        cal = Calendar.getInstance();
        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH);
        year = cal.get(Calendar.YEAR);
        hour = cal.get(Calendar.HOUR_OF_DAY);
        min = cal.get(Calendar.MINUTE);
        restAdapter= new RestAdapter.Builder()
            .setEndpoint(Verification.END_POINT)
            .build();
     }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
       id_variable=Session.list_variable.get(position).getId();
       name_varible_selec=Session.list_variable.get(position).getName();
    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {}

    public void realTime(View view){
        intent=new Intent(this,Real_Time.class);
        finish();
        startActivity(intent);
        overridePendingTransition(R.anim.abc_fade_in,R.anim.abc_fade_out);
    }

    public void notification(View view){
        intent=new Intent(this,Notifications.class);
        finish();
        startActivity(intent);
        overridePendingTransition(R.anim.abc_fade_in,R.anim.abc_fade_out);
    }

    public void configure(View view){
        intent=new Intent(this,Configuration.class);
        finish();
        startActivity(intent);
        overridePendingTransition(R.anim.abc_fade_in,R.anim.abc_fade_out);
    }

    public void selec_watch1(View view){
       f=1;
        showDialog(1);
    }

    public void selec_date1(View view){
        f=1;
        showDialog(0);
    }

    public void selec_watch2(View view){
        f=2;
        showDialog(1);
    }

    public void selec_date2(View view){
        f=2;
        showDialog(0);
    }

    @Override
    @Deprecated

    protected Dialog onCreateDialog(int id) {
        if(id==1){
            return new TimePickerDialog(this, timePickerListener, hour, min, false);
        }
        else {
            return new DatePickerDialog(this, datePickerListener, year, month, day);
        }
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            if(f==1) {
                et.setText(selectedDay + " / " + (selectedMonth + 1) + " / "
                        + selectedYear);
                dateInString1 = Integer.toString(selectedYear) + "-" + Integer.toString(selectedMonth+1) + "-" + Integer.toString(selectedDay);
            }else{
                ef.setText(selectedDay + " / " + (selectedMonth + 1) + " / "
                        + selectedYear);
               dateInString2=Integer.toString(selectedYear)+"-"+Integer.toString(selectedMonth+1)+"-"+Integer.toString(selectedDay);
            }
        }
    };
    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            if(f==1) {
                et1.setText(hourOfDay + ":" + minute);
                timeInString1 = Integer.toString(hourOfDay) + ":" + Integer.toString(minute);// + ":00.000";
            }else {
                ef1.setText(hourOfDay+":"+minute);
                timeInString2 = Integer.toString(hourOfDay) + ":" + Integer.toString(minute);// + ":00.000";
            }
        }
    };

    public void init_search(View view) throws ParseException {
        cal = Calendar.getInstance();
        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH);
        year = cal.get(Calendar.YEAR);
        hour = cal.get(Calendar.HOUR_OF_DAY);
        min = cal.get(Calendar.MINUTE);
        if(et.getText().toString().equals("")||et1.getText().toString().equals("")||ef.getText().toString().equals("")||ef1.getText().equals("")){
            Toast.makeText(this,"Debe llenar todos los campos",Toast.LENGTH_SHORT).show();
        }else{
            String d1=dateInString1+" "+timeInString1,d2=dateInString2+" "+timeInString2;
            actual= Integer.toString(year) + "-" + Integer.toString(month+1) + "-" + Integer.toString(day)+" "+Integer.toString(hour) + ":" + Integer.toString(min);
            Date date1 = sdf.parse(d1),date2=sdf.parse(d2),date3=sdf.parse(actual);
            long longDate1=date1.getTime(),longDate2=date2.getTime(),longDate3=date3.getTime();
            if(date1.before(date2)){
                if(date2.before(date3)) {
                    setContentView(R.layout.loading);
                    TextView title = (TextView) findViewById(R.id.textView_veryfi);
                    title.setText("Buscando");
                    ProcessAPI processAPI = restAdapter.create(ProcessAPI.class);
                    processAPI.ViewCapture2(id_variable, longDate1, longDate2, new Callback<List<Capture_Variable>>() {
                        @Override
                        public void success(List<Capture_Variable> capture_variables, Response response) {
                            value = new String[3 * capture_variables.size()];
                            num = new int[capture_variables.size()];
                            int h = 0;
                            for (int i = 0; i < capture_variables.size(); i++) {
                                Date datex = new Date(capture_variables.get(i).getDate());
                                value[h] = Integer.toString(capture_variables.get(i).getValue());
                                num[i] = capture_variables.get(i).getValue();
                                value[h + 1] = sdf1.format(datex);
                                value[h + 2] = sdf2.format(datex);
                                h = h + 3;
                            }
                            viewListValue();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.d("retrofit", error.toString());
                        }
                    });
                }else{ Toast.makeText(this,"Error, la segunda fecha debe ser menor a la fecha actual",Toast.LENGTH_SHORT).show();}
            }else{
                Toast.makeText(this,"Error, la segunda fecha debe ser mayor a la primera ",Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void viewListValue(){
        setContentView(R.layout.search_list);
        adapter1=new ArrayAdapter<String>(this,android.R.layout.simple_gallery_item,value);
        TextView variable=(TextView)findViewById(R.id.name_variable_search);
        variable.setText(name_varible_selec);
        GridView gv= (GridView) findViewById(R.id.gridView);
        gv.setAdapter(adapter1);

    }

    public void return_search_list(View view){
        configureInit();
    }

    private void configureInit(){
        setContentView(R.layout.search);
        et = (EditText) findViewById(R.id.date1);
        et1 = (EditText) findViewById(R.id.time1);
        ef = (EditText) findViewById(R.id.date2);
        ef1 = (EditText) findViewById(R.id.time2);
        spinner=(Spinner)findViewById(R.id.spinner2);
        adapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,Session.name_v);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
    }

    public void viewGraphic(View view) {
        setContentView(R.layout.search_graphical);
        GraphView graph = (GraphView) findViewById(R.id.graph);
        TextView variable=(TextView)findViewById(R.id.name_variable_search);
        variable.setText(name_varible_selec);
        LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>(generateData());
        graph.addSeries(series);
        graph.getGridLabelRenderer().setHorizontalLabelsColor(Color.RED);
        Viewport viewport=graph.getViewport();
        viewport.setScrollable(true);
        viewport.setScalable(true);
    }

    private DataPoint[] generateData() {
        DataPoint[] values = new DataPoint[num.length];
        for (int i=0; i<num.length; i++) {
            double x = i;
            double y = num[i];
            DataPoint v = new DataPoint(x, y);
            values[i] = v;
        }
        return values;
    }

    public  void viewList(View view){
        viewListValue();
    }
}


