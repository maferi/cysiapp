package com.app.cysiapp.activi;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;
import com.app.cysiapp.R;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.app.PendingIntent.getActivity;


public class Verification extends ActionBarActivity {

    public static final String END_POINT="http://192.168.0.103:8080/cysi";
    public static String ubication = "tesis.org.cysi.activi";
    AlertDialog.Builder builder;
    Intent validate,registration,sesion,login;
    RestAdapter restAdapter;
    String info[];
    Toast toast;
    public static long id_code;
    public static String[] name_process,list_code;
    public static long[] id_process;
    public static List<Process> list_process;
    public static String rol;
    public static int[] img;
    DatabaseHandler db=new DatabaseHandler(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading);

        validate=getIntent();
        info=validate.getStringArrayExtra(ubication);

        registration=new Intent(this,Registration.class);
        login=new Intent(this,Login.class);
        sesion=new Intent(this,Session.class);

        toast = Toast.makeText(this, info[2], Toast.LENGTH_SHORT);
        restAdapter= new RestAdapter.Builder().setEndpoint(END_POINT).build();

        builder = new AlertDialog.Builder(this);
        builder.setTitle(" Información")
               .setIcon(getResources().getDrawable(android.R.drawable.ic_dialog_info))
               .setNeutralButton(android.R.string.ok,
                       new DialogInterface.OnClickListener() {
                           public void onClick(DialogInterface dialog, int id) {
                               finish();
                               startActivity(login);
                           }
                       });


        if(info[2].equals("check")){verification();}
        else if(info[2].equals("start")){sesion();}
        else{session2();}

    }

    private void verification(){
        ReaderAPI api = restAdapter.create(ReaderAPI.class);
        api.Verification(info[0],info[1],new Callback<ServerResponse>() {
            @Override
            public void success(ServerResponse serverResponse, Response response) {
                if(serverResponse.isOk()) {
                    id_code=serverResponse.getId();
                    registration.putExtra(ubication,info[0]);
                    startActivity(registration);
                    finish();
                }else{
                    builder.setMessage(serverResponse.getMessage()).create().show();
                }
            }
            @Override
            public void failure(RetrofitError error) {Log.d("retrofit", error.toString());}
        });
    }

    private void sesion(){
        ReaderAPI api = restAdapter.create(ReaderAPI.class);
        api.StartSesion(info[0],info[1],new Callback<ServerResponse>() {
            @Override
            public void success(ServerResponse serverResponse, Response response) {
                if(serverResponse.isOk()) {
                    rol = serverResponse.getMessage();
                    session2();
                }
                else{
                    startActivity(login);
                    finish();
                    toast.setText(serverResponse.getMessage());
                    toast.show();
                }
            }
            @Override
            public void failure(RetrofitError error) {
                Log.d("retrofit", error.toString());
                toast.setText("No se ha podido establecer la conexión con el servidor");
                toast.show();
                startActivity(login);
                finish();

            }});
    }


    private void session2(){
        final ProcessAPI processAPI  = restAdapter.create(ProcessAPI.class);
        processAPI.ViewListProcess(info[0], new Callback<List<Process>>() {

            @Override
            public void success(List<Process> processes, Response response) {
                list_process = processes;
                name_process = new String[processes.size()];
                id_process = new long[processes.size()];
                for (int i = 0; i < processes.size(); i++) {
                    name_process[i] = processes.get(i).getName();
                    id_process[i] = processes.get(i).getId();
                }
                processAPI.ViewCode(new Callback<List<Code>>() {
                    @Override
                    public void success(List<Code> codes, Response response) {
                        list_code = new String[codes.size()];
                        img = new int[codes.size()];
                        for (int i = 0; i < list_code.length; i++) {
                            list_code[i] = codes.get(i).getKey();
                            if (codes.get(i).isActive()) {
                                img[i] = R.drawable.busy;
                            } else {
                                img[i] = R.drawable.a;
                            }
                        }
                        sesion.putExtra(Verification.ubication, "selec");
                        db.updateContact(new Contact(1, info[0], info[1], rol, "si"));
                        startActivity(sesion);
                        finish();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                    }
                });
            }
            @Override
            public void failure(RetrofitError error) {
                Log.d("retrofit", error.toString());
                toast.setText("No se ha podido establecer la conexión con el servidor");
                toast.show();
                finish();
            }});
    }

}
