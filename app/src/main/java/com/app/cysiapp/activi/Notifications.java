package com.app.cysiapp.activi;

import android.app.Activity;
import android.app.Notification;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.app.cysiapp.R;

/**
 * Created by USUARIO on 24/05/2015.
 */
public class Notifications extends Activity {
    Intent intent;
    private ListView list;
    ListViewVariableAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notifications);
        final Intent intent2 = new Intent(Notifications.this,Servicio.class);
        listView();
        cancelNotification();

}

    public void listView(){
        final GlobalClass globalVariable = (GlobalClass) getApplicationContext();
        list = (ListView)findViewById(R.id.listView);
        if(globalVariable.getVariables().size()!=0){
            adapter = new ListViewVariableAdapter(this, globalVariable.getVariables(),globalVariable.getImagen());
            list.setAdapter(adapter);
        }
    };

    @Override
    protected void onDestroy() {
        this.cancelNotification();
        super.onDestroy();
    }

    public void cancelNotification(){
        final GlobalClass globalVariable = (GlobalClass) getApplicationContext();
        globalVariable.cancelNotification();
    }

    public void realTime(View view){
        intent=new Intent(this,Real_Time.class);
        finish();
        startActivity(intent);
        overridePendingTransition(R.anim.abc_fade_in,R.anim.abc_fade_out);

    }

    public void dataBase(View view){
        intent=new Intent(this,Data_Base.class);
        finish();
        startActivity(intent);
        overridePendingTransition(R.anim.abc_fade_in,R.anim.abc_fade_out);

    }

    public void configure(View view){
        intent=new Intent(this,Configuration.class);
        finish();
        startActivity(intent);
        overridePendingTransition(R.anim.abc_fade_in,R.anim.abc_fade_out);

    }

}
