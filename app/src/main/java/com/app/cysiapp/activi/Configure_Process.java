package com.app.cysiapp.activi;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.cysiapp.R;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by USUARIO on 27/05/2015.
 */
public class Configure_Process extends ActionBarActivity {
    ArrayAdapter<String> adapter;
    AlertDialog.Builder builder;
    RestAdapter restAdapter;
    Intent intent,entry;
    Toast toast;
    long id_process;
    int process_selec,indicator;
    TextView title;
    EditText name,ip,slave,name_pl,description,init,quantity,initb,quantityb;
    String name_plc,edit,message, name_process=Verification.name_process[Session.position_clik],a;
    boolean type=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        entry=getIntent();
        a=entry.getStringExtra(Verification.ubication);
        toast=Toast.makeText(this,"",Toast.LENGTH_LONG);
        restAdapter= new RestAdapter.Builder()
                .setEndpoint(Verification.END_POINT)
                .build();
        builder = new AlertDialog.Builder(this);
        builder.setTitle(" Advertencia")
                .setIcon(getResources().getDrawable(android.R.drawable.ic_dialog_alert))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        setContentView(R.layout.loading);
                        title=(TextView)findViewById(R.id.textView_veryfi);
                        title.setText("Eliminando");
                        ProcessAPI processAPI  = restAdapter.create(ProcessAPI.class);
                        processAPI.RemoveProcess(id_process, new Callback<List<Process>>() {
                            @Override
                            public void success(List<Process> processes, Response response) {
                                Verification.list_process=processes;
                                Verification.name_process=new String[processes.size()];
                                Verification.id_process=new long[processes.size()];
                                for (int i=0;i<processes.size();i++){
                                    if(processes.get(i).getId()==Session.p){Session.position_clik=i;}
                                    Verification.name_process[i]=processes.get(i).getName();
                                    Verification.id_process[i]=processes.get(i).getId();
                                }
                                finish();
                            }
                            @Override
                            public void failure(RetrofitError error) {Log.d("retrofit", error.toString());}
                        });
                    }
                }).setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            dialog.cancel();
            }
        });

        if(a.equals("see")){
            seeProcess();
        }else{
            newPlc();
        }

    }

    public void seeProcess(){
        indicator=0;
        setContentView(R.layout.view_list_process);
        TextView title=(TextView)findViewById(R.id.titleViewListProcess);
        title.setText("Procesos");
        intent=new Intent(this,Configuration.class);
        adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,Verification.name_process);
        ListView lv= (ListView) findViewById(R.id.listViewProcess);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                process_selec=position;
                viewProcess(position);
            }});
    }

    public void viewProcess(int position){
        indicator=2;
        setContentView(R.layout.view_process);
        TextView title2=(TextView)findViewById(R.id.title_view_process);
        title2.setText(Verification.list_process.get(position).getName());
        TextView init=(TextView)findViewById(R.id.view_init);
        TextView quantity=(TextView)findViewById(R.id.view_quantity);
        TextView plc=(TextView)findViewById(R.id.name_plc);
        TextView ip=(TextView)findViewById(R.id.ip_plc);
        TextView slave=(TextView)findViewById(R.id.slave_plc);
        TextView initb=(TextView)findViewById(R.id.textView39);
        TextView quantityb=(TextView)findViewById(R.id.textView47);
        init.setText(Integer.toString(Verification.list_process.get(position).getInit()));
        quantity.setText(Integer.toString(Verification.list_process.get(position).getQuantity()));
        initb.setText(Integer.toString(Verification.list_process.get(position).getInitb()));
        quantityb.setText(Integer.toString(Verification.list_process.get(position).getQuantityb()));
        plc.setText(Verification.list_process.get(position).getPlc().getName());
        ip.setText(Verification.list_process.get(position).getPlc().getIp());
        slave.setText(Integer.toString(Verification.list_process.get(position).getPlc().getSlave()));
        id_process=Verification.list_process.get(position).getId();
        }

    public void leaveViewListProcess(View view){ finish();}

    public void leaveViewProcess(View view){seeProcess();}

    public void addVariable(View view){
        intent=new Intent(this,Configure_Variable.class);
        intent.putExtra(Verification.ubication,Long.toString(id_process));
        startActivity(intent);
    }

    public void deleteProcess(View view){
      if(Verification.list_process.get(process_selec).getId()==1){toast.setText("Este proceso no puede ser eliminado"); toast.show();}
        else{
            if(Verification.list_process.get(process_selec).getId()!=Session.p){builder.setMessage("¿Seguro que desea eliminar el proceso: " + Verification.list_process.get(process_selec).getName() + " ?").show();}
            else{toast.setText("No puede eliminar, el proceso se encuentra en supervisión de Tiempo Real"); toast.show();}
      }

    }

    private void newPlc(){
        indicator=0;
        setContentView(R.layout.configure_plc);
        name=(EditText)findViewById(R.id.insert_name_plc);
        ip=(EditText)findViewById(R.id.insert_ip);
        slave=(EditText)findViewById(R.id.insert_slave);
    }

    public void cancelConfigPlc(View view){if(a.equals("see")){viewProcess(process_selec);}else{finish();}}

    public void savePlc(View view){
        final String name_p=name.getText().toString(),ip_p=ip.getText().toString(),slave_p=slave.getText().toString();
        if(name_p.equals("")||ip_p.equals("")||slave_p.equals("")){
            toast.setText("Todos los campos son obligatorios");
            toast.show();
        }else{
            setContentView(R.layout.loading);
            title = (TextView) findViewById(R.id.textView_veryfi);
            title.setText("Guardando Plc");
            name_plc=name_p;
            ProcessAPI processAPI = restAdapter.create(ProcessAPI.class);
           if(type==false){processAPI.ConfigurePlc(name_p,ip_p,Integer.parseInt(slave_p),new Callback<ServerResponse>() {
                @Override
                public void success(ServerResponse serverResponse, Response response) {
                    if(serverResponse.isOk()){
                        newPrcoess();
                    }else{
                      newPlc();
                      name.setText(name_p);
                      ip.setText(ip_p);
                      slave.setText(slave_p);
                      toast.setText(serverResponse.getMessage());
                      toast.show();
                    }
                }
                @Override
                public void failure(RetrofitError error){Log.d("retrofit", error.toString());}
            });}
           else {processAPI.EditPlc(Verification.list_process.get(process_selec).getPlc().getName(),name_p,ip_p,Integer.parseInt(slave_p),new Callback<ServerResponse>() {
                   @Override
                   public void success(ServerResponse serverResponse, Response response) {
                       if(serverResponse.isOk()){
                           newPrcoess();
                       }else{
                           newPlc();
                           name.setText(name_p);
                           ip.setText(ip_p);
                           slave.setText(slave_p);
                           toast.setText(serverResponse.getMessage());
                           toast.show();
                       }
                   }
                   @Override
                   public void failure(RetrofitError error) {}
               });}
        }
    }

    private void newPrcoess() {
        indicator=3;
        setContentView(R.layout.configure_process);
        name_pl=(EditText)findViewById(R.id.insert_name_process);
        description=(EditText)findViewById(R.id.insert_description);
        init=(EditText)findViewById(R.id.insert_init_process);
        quantity=(EditText)findViewById(R.id.insert_quantity_process);
        initb=(EditText)findViewById(R.id.insert_initb_process);
        quantityb=(EditText)findViewById(R.id.insert_quantityb_process);
        if(type){
            indicator=4;
            name_pl.setText(Verification.list_process.get(process_selec).getName());
            description.setText(Verification.list_process.get(process_selec).description);
            init.setText(Integer.toString(Verification.list_process.get(process_selec).getInit()));
            quantity.setText(Integer.toString(Verification.list_process.get(process_selec).getQuantity()));
            initb.setText(Integer.toString(Verification.list_process.get(process_selec).getInitb()));
            quantityb.setText(Integer.toString(Verification.list_process.get(process_selec).getQuantityb()));
        }
    }

    public void cancelConfigProceso(View view){
        if(a.equals("new")){
            builder.setMessage("Al salir de la configuración del proceso se eliminaran los datos del plc previamente configurado")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        setContentView(R.layout.loading);
                        title=(TextView)findViewById(R.id.textView_veryfi);
                        title.setText("Eliminando Plc");
                        ProcessAPI processAPI = restAdapter.create(ProcessAPI.class);
                        processAPI.DeletePlc(name_plc,new Callback<ServerResponse>() {
                            @Override
                            public void success(ServerResponse serverResponse, Response response) {finish();}
                            @Override
                            public void failure(RetrofitError error) {

                            }
                        });
                    }
                }).setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                     public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();}
        }).show();
        }else{
            Toast.makeText(this, "Debe aceptar la configuración", Toast.LENGTH_SHORT).show();
        }
    }

    public void saveProcess(View view) {
       final String name_p=name_pl.getText().toString(),description_p=description.getText().toString(),init_p=init.getText().toString(),quantity_p=quantity.getText().toString(),initb_p=initb.getText().toString(),quantityb_p=quantityb.getText().toString();
        if (name_p.equals("") || init_p.equals("") || quantity_p.equals("")||initb_p.equals("")||quantityb_p.equals("")) {
            toast.setText("Todos los campos excepto la descripción son obligatorio");
            toast.show();
        } else {
            setContentView(R.layout.loading);
            title = (TextView) findViewById(R.id.textView_veryfi);
            title.setText("Guardando proceso");
            final ProcessAPI processAPI = restAdapter.create(ProcessAPI.class);
            if(type==false){
                type=false;
                processAPI.ConfigureProcess(name_p,description_p,Integer.parseInt(init_p),Integer.parseInt(quantity_p),Integer.parseInt(initb_p),Integer.parseInt(quantityb_p),name_plc,new Callback<ServerResponse>() {
                    @Override
                    public void success(ServerResponse serverResponse, Response response) {
                        toast.setText(serverResponse.getMessage());
                        toast.show();
                        if (serverResponse.isOk()) {
                            processAPI.ViewAllProcess(new Callback<List<Process>>() {
                                @Override
                                public void success(List<Process> processes, Response response) {
                                    Verification.list_process = processes;
                                    Verification.name_process = new String[processes.size()];
                                    Verification.id_process=new long[processes.size()];
                                    for (int i = 0; i < processes.size(); i++) {
                                        if(processes.get(i).getId()==Session.p){Session.position_clik=i;}
                                        Verification.name_process[i] = processes.get(i).getName();
                                        Verification.id_process[i]=processes.get(i).getId();
                                    }

                                    finish();
                                }
                                @Override
                                public void failure(RetrofitError error) {
                                }
                            });
                        } else {
                            newPrcoess();
                            name_pl.setText(name_p);
                            description.setText(description_p);
                            init.setText(init_p);
                            quantity.setText(quantity_p);
                            initb.setText(initb_p);
                            quantityb.setText(quantityb_p);
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {}
                });}
            else{
                processAPI.EditProcess(Verification.list_process.get(process_selec).getName(),name_p,description_p,Integer.parseInt(init_p),Integer.parseInt(quantity_p),Integer.parseInt(initb_p),Integer.parseInt(quantityb_p),name_plc,new Callback<ServerResponse>() {
                    @Override
                    public void success(ServerResponse serverResponse, Response response) {
                        message=serverResponse.getMessage();
                        if (serverResponse.isOk()) {processAPI.ViewAllProcess(new Callback<List<Process>>() {
                                @Override
                                public void success(List<Process> processes, Response response) {
                                    Verification.list_process = processes;
                                    Verification.name_process = new String[processes.size()];
                                    for (int i = 0; i < processes.size(); i++) {
                                        if(processes.get(i).getId()==Session.p){Session.position_clik=i;}
                                          Verification.name_process[i] = processes.get(i).getName();
                                    }
                                    if (Session.p==id_process){
                                        Session.list_variable = processes.get(Session.position_clik).getVariable();
                                        Session.name_value_variable=new String[Session.list_variable.size()];
                                        Session.name_v=new String[Session.list_variable.size()];
                                        processAPI.RealTime(Session.p,new Callback<ServerResponse>() {
                                            @Override
                                            public void success(ServerResponse serverResponse, Response response) {
                                                for(int i=0;i<Session.list_variable.size();i++){
                                                    Session.name_v[i]=Session.list_variable.get(i).getName();
                                                    if(Session.list_variable.get(i).getType().equals("digital")){ Session.name_value_variable[i]=Session.list_variable.get(i).getName()+System.getProperty(("line.separator"))+Integer.toString(serverResponse.getValue()[Verification.list_process.get(Session.position_clik).getQuantity()+Session.list_variable.get(i).getPosition()]);
                                                    }else{Session.name_value_variable[i]=Session.list_variable.get(i).getName()+System.getProperty(("line.separator"))+Integer.toString(serverResponse.getValue()[Session.list_variable.get(i).getPosition()]);}
                                                    finish();
                                                    toast.setText("Configuración exitosa");
                                                    toast.show();
                                                }
                                            }
                                            @Override
                                            public void failure(RetrofitError error) {}});
                                    }else{finish();
                                        toast.setText("Configuración exitosa");
                                        toast.show();}



                                }
                                @Override
                                public void failure(RetrofitError error) { Log.d("retrofit", error.toString());}});
                        }
                        else {
                            toast.setText(message);
                            toast.show();
                            newPrcoess();
                            name_pl.setText(name_p);
                            description.setText(description_p);
                            init.setText(init_p);
                            quantity.setText(quantity_p);
                            initb.setText(initb_p);
                            quantityb.setText(quantityb_p);
                        }
                    }
                    @Override
                    public void failure(RetrofitError error) { Log.d("retrofit", error.toString());}});
            }
        }
    }

    public void editProcess(View view){
        type=true;
        edit="edit";
        newPlc();
        indicator=1;
        name.setText(Verification.list_process.get(process_selec).getPlc().getName());
        ip.setText(Verification.list_process.get(process_selec).getPlc().getIp());
        slave.setText(Integer.toString(Verification.list_process.get(process_selec).getPlc().getSlave()));
    }

    public void onBackPressed(){
        if(indicator==0){finish();}
        else if(indicator==1){viewProcess(process_selec);}
        else if(indicator==2){seeProcess();}
        else if(indicator==3){ builder.setMessage("Al salir de la configuración del proceso se eliminaran los datos del plc previamente configurado")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        setContentView(R.layout.loading);
                        title=(TextView)findViewById(R.id.textView_veryfi);
                        title.setText("Eliminando Plc");
                        ProcessAPI processAPI = restAdapter.create(ProcessAPI.class);
                        processAPI.DeletePlc(name_plc,new Callback<ServerResponse>() {
                            @Override
                            public void success(ServerResponse serverResponse, Response response) {finish();}
                            @Override
                            public void failure(RetrofitError error) {

                            }
                        });
                    }
                }).setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();}
                }).show();}

       else if (indicator==4){Toast.makeText(this, "Debe aceptar la configuración", Toast.LENGTH_SHORT).show();}

    }
}



