package com.app.cysiapp.activi;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.app.cysiapp.R;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by USUARIO on 24/05/2015.
 */
public class View_Variable extends ActionBarActivity {
    RestAdapter restAdapter;
    Intent intent,config_variable;
    AlertDialog.Builder builder;
    TextView textViewVariable,textViewPosicionVariable,textViewSet,textViewLow,textViewHigh,title,textViewType,textSet,textLow,textHigh;
    boolean end=true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_variable);
        intent=new Intent(this,Real_Time.class);
        config_variable=new Intent(this,Configure_Variable.class);

        restAdapter= new RestAdapter.Builder()
                .setEndpoint(Verification.END_POINT)
                .build();
        final ProcessAPI processAPI  = restAdapter.create(ProcessAPI.class);
        builder = new AlertDialog.Builder(this);
        builder.setTitle(" Advertencia")
                .setMessage("¿Seguro que desea eliminar la variable: " + Session.list_variable.get(Real_Time.posición_variable).getName() + " ?")
                .setIcon(getResources().getDrawable(android.R.drawable.ic_dialog_alert))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                       setContentView(R.layout.loading);
                       title=(TextView)findViewById(R.id.textView_veryfi);
                       title.setText("Eliminando");
                       processAPI.RemoveVariable(Session.list_variable.get(Real_Time.posición_variable).getId(),new Callback<List<Variable>>() {
                           @Override
                           public void success(List<Variable> variables, Response response) {
                               Session.list_variable.clear();
                               Session.list_variable=variables;
                               Session.name_value_variable=new String[Session.list_variable.size()];
                                   for(int i=0;i< Session.list_variable.size();i++){
                                       Session.name_value_variable[i]= Session.list_variable.get(i).getName()+
                                               System.getProperty(("line.separator"))+
                                               Integer.toString(1);
                                   }
                                   startActivity(intent);
                                   finish();
                           }
                           @Override
                           public void failure(RetrofitError error) {Log.d("retrofit", error.toString());}
                       });
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });


        textViewVariable=(TextView)findViewById(R.id.variable);
        textViewType=(TextView)findViewById(R.id.textViewtype);
        textViewPosicionVariable=(TextView)findViewById(R.id.posicionVariable);
        textViewSet=(TextView)findViewById(R.id.set);
        textSet=(TextView)findViewById(R.id.textView33);
        textLow=(TextView)findViewById(R.id.textView34);
        textHigh=(TextView)findViewById(R.id.textView36);
        textViewLow=(TextView)findViewById(R.id.low);
        textViewHigh=(TextView)findViewById(R.id.high);

        textViewVariable.setText(Session.list_variable.get(Real_Time.posición_variable).getName());
        textViewPosicionVariable.setText(Integer.toString(Session.list_variable.get(Real_Time.posición_variable).getPosition()));
        textViewType.setText(Session.list_variable.get(Real_Time.posición_variable).getType());
        if(Session.list_variable.get(Real_Time.posición_variable).getType().equals("analógica")){
            textSet.setVisibility(View.VISIBLE);
            textLow.setVisibility(View.VISIBLE);
            textHigh.setVisibility(View.VISIBLE);
            textViewSet.setVisibility(View.VISIBLE);
            textViewLow.setVisibility(View.VISIBLE);
            textViewHigh.setVisibility(View.VISIBLE);
        }
        List<Point> list_point=Session.list_variable.get(Real_Time.posición_variable).getPoint();
          for(int p=0;p<list_point.size();p++){
           if(list_point.get(p).getPosition()!=-1){
             if(list_point.get(p).getType().equals("set")){textViewSet.setText(Integer.toString(list_point.get(p).getValue()));}
             else if(list_point.get(p).getType().equals("low")){textViewLow.setText(Integer.toString(list_point.get(p).getValue()));}
             else{textViewHigh.setText(Integer.toString(list_point.get(p).getValue()));}
           }
        }
    }

    public void backViewVariable(View view){finish(); startActivity(intent);}

    public void editVariable(View view){
        end=false;
        config_variable.putExtra(Verification.ubication,"edit");
        finish();
        startActivity(config_variable);
    }

    public void removeVariable(View view){
        builder.show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
         if(keyCode==KeyEvent.KEYCODE_BACK){finish(); startActivity(intent);}
         return super.onKeyDown(keyCode, event);
    }

}

