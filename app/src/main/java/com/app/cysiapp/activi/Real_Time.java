package com.app.cysiapp.activi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.app.cysiapp.R;

import java.util.Timer;
import java.util.TimerTask;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by USUARIO on 21/05/2015.
 */
public class Real_Time  extends ActionBarActivity {
    GridView gridVariable;
    ArrayAdapter<String> adapter;
    private Timer timer;
    RestAdapter restAdapter;
    Intent intent, variable;
    TextView textView;
    public  static int posición_variable;
    Sub_Real_Time sub_real_time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.real_time_monitor);
        variable=new Intent(this,View_Variable.class);
        restAdapter= new RestAdapter.Builder()
                .setEndpoint(Verification.END_POINT)
                .build();
        timer=new Timer();
        sub_real_time=new Sub_Real_Time();
        textView=(TextView)findViewById(R.id.textView_real);
        textView.setText(Verification.name_process[Session.position_clik]);

        gridVariable= (GridView) findViewById(R.id.gridview_variable);
        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1);
        adapter.clear();
        for(int i=0;i<Session.name_value_variable.length;i++){
            adapter.add(Session.name_value_variable[i]);
        }

        gridVariable.setAdapter(adapter);
        timer.scheduleAtFixedRate(sub_real_time,0,2000);

        if(Verification.rol.equals("Admin")) {
            gridVariable.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    posición_variable = position;
                    finish();
                    timer.cancel();
                    startActivity(variable);
                }
            });
        }

        gridVariable.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                intent=new Intent(getApplication(),Edit_Value_Real_Time.class);
                intent.putExtra(Verification.ubication,Session.list_variable.get(position).getId());
                startActivity(intent);
                return false;
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }

    class Sub_Real_Time extends TimerTask{
        @Override
        public void run() {
            ProcessAPI processAPI  = restAdapter.create(ProcessAPI.class);
            processAPI.RealTime(Session.p,new Callback<ServerResponse>() {
                @Override
                public void success(ServerResponse serverResponse, Response response) {
                    adapter.clear();
                    for (int i = 0; i < Session.list_variable.size(); i++) {
                        Session.name_v[i] = Session.list_variable.get(i).getName();
                        if (Session.list_variable.get(i).getType().equals("digital")) {
                            Session.name_value_variable[i] = Session.list_variable.get(i).getName() +
                                    System.getProperty(("line.separator")) +
                                    Integer.toString(serverResponse.getValue()[Verification.list_process.get(Session.position_clik).getQuantity() + Session.list_variable.get(i).getPosition()]);
                        } else {
                            Session.name_value_variable[i] = Session.list_variable.get(i).getName() +
                                    System.getProperty(("line.separator")) +
                                    Integer.toString(serverResponse.getValue()[Session.list_variable.get(i).getPosition()]);
                        }
                        adapter.add(Session.name_value_variable[i]);
                    }

                }
                @Override
                public void failure(RetrofitError error) {
                    Log.d("retrofit", error.toString());
                }
            });


        }
    }

    public void dataBase(View view){
        intent=new Intent(this,Data_Base.class);
        finish();
        startActivity(intent);
        overridePendingTransition(R.anim.abc_fade_in,R.anim.abc_fade_out);
    }

    public void notification(View view){
        intent=new Intent(this,Notifications.class);
        finish();
        startActivity(intent);
        overridePendingTransition(R.anim.abc_fade_in,R.anim.abc_fade_out);
    }

    public void configure(View view){
        intent=new Intent(this,Configuration.class);
        finish();
        startActivity(intent);
        overridePendingTransition(R.anim.abc_fade_in,R.anim.abc_fade_out);
    }

    public void selecProcess(View view){
        timer.cancel();
        intent=new Intent(this,Session.class);
        finish();
        intent.putExtra(Verification.ubication,"change");
        startActivity(intent);
    }

}
