package com.app.cysiapp.activi;

/**
 * Created by USUARIO on 27/05/2015.
 */
public class Plc {

    private String name;
    private int slave;
    private String ip;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSlave() {
        return slave;
    }

    public void setSlave(int slave) {
        this.slave = slave;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
