package com.app.cysiapp.activi;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.app.cysiapp.R;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by USUARIO on 19/05/2015.
 */
public class Recovery extends ActionBarActivity implements AdapterView.OnItemSelectedListener {

    Spinner spinner;
    private String type, email;
    RestAdapter restAdapter;
    Toast toast;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recovery);
        spinner=(Spinner)findViewById(R.id.spinner);
        ArrayAdapter adapter=ArrayAdapter.createFromResource(this,R.array.option_recovery,android.R.layout.simple_spinner_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        restAdapter= new RestAdapter.Builder()
                .setEndpoint(Verification.END_POINT)
                .build();
        toast=Toast.makeText(this, "", Toast.LENGTH_LONG);

     }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        TextView myText=(TextView) view;

        if(myText.getText().toString().equals("Usuario")){type="reader";}
        else if(myText.getText().toString().equals("Contraseña")){type="password";}
        else{type=myText.getText().toString();}
     }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void recovery(View view){
       email=((EditText)findViewById(R.id.email)).getText().toString();
       if (email.equals("")||type.equals("Recuperar:")){
           Toast.makeText(this,"Debe completar la información",Toast.LENGTH_SHORT).show();
       }else{
           setContentView(R.layout.loading);
           TextView textView=(TextView)findViewById(R.id.textView_veryfi);
           textView.setText("Recuperando");
           ReaderAPI api = restAdapter.create(ReaderAPI.class);
           api.Recovery(type,email,new Callback<ServerResponse>() {
               @Override
               public void success(ServerResponse serverResponse, Response response) {
                   toast.setText(serverResponse.getMessage());
                   toast.show();
                   finish();
               }
               @Override
               public void failure(RetrofitError error) {Log.d("retrofit", error.toString());}
           });}
    }

    public void back(View view){
        finish();
    }
}
