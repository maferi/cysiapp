package com.app.cysiapp.activi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.TextView;

import com.app.cysiapp.R;

/**
 * Created by USUARIO on 24/05/2015.
 */
public class Configuration extends ActionBarActivity {
    Intent intent,config_process,config_access, intent2;
    TextView process,view_process,new_process,access,view_access,new_access;
    DatabaseHandler db=new DatabaseHandler(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.configuration);
        process=(TextView)findViewById(R.id.view_process_configure);
        view_process=(TextView)findViewById(R.id.view_process);
        new_process=(TextView)findViewById(R.id.new_process);
        access=(TextView)findViewById(R.id.view_access_configure);
        view_access=(TextView)findViewById(R.id.view_access);
        new_access=(TextView)findViewById(R.id.new_access);
        intent2 = new Intent(this,Servicio.class);

        if(Verification.rol.equals("Admin")) {
            config_process = new Intent(this, Configure_Process.class);
            view_process.setText("Ver procesos");
            new_process.setText("Nuevo proceso");
            view_access.setText("Ver accesos");
            new_access.setText("Nuevo acceso");
            config_access = new Intent(this, Configure_Access.class);
            process.setText("Proceso");
            access.setText("Acceso");
            view_process.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    config_process.putExtra(Verification.ubication, "see");
                    startActivity(config_process);
                }
            });

            new_process.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    config_process.putExtra(Verification.ubication, "new");
                    startActivity(config_process);
                }
            });

            new_access.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    config_access.putExtra(Verification.ubication, "new");
                    startActivity(config_access);
                }
            });

            view_access.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    config_access.putExtra(Verification.ubication, "see");
                    startActivity(config_access);
                }
            });

        }

    }

    public void realTime(View view){
        intent=new Intent(this,Real_Time.class);
        finish();
        startActivity(intent);
        overridePendingTransition(R.anim.abc_fade_in,R.anim.abc_fade_out);
    }

    public void dataBase(View view){
        intent=new Intent(this,Data_Base.class);
        finish();
        startActivity(intent);
        overridePendingTransition(R.anim.abc_fade_in,R.anim.abc_fade_out);
    }

    public void notification(View view){
        intent=new Intent(this,Notifications.class);
        finish();
        startActivity(intent);
        overridePendingTransition(R.anim.abc_fade_in,R.anim.abc_fade_out);
    }

    public void selecChangePassword(View view){
        intent=new Intent(this,Configure_User.class);
        intent.putExtra(Verification.ubication,"password");
        startActivity(intent);
    }

    public void selecChangeMail(View view){
        intent=new Intent(this,Configure_User.class);
        intent.putExtra(Verification.ubication,"email");
        startActivity(intent);
    }

    public void closeSession (View view){
        db.updateContact(new Contact(1,"no"));
        intent=new Intent(this,Login.class);
        stopService(intent2);
        finish();
        startActivity(intent);
    }

}
